'''
Created on Mar 27, 2016

@author: Alex
'''

import click
from geometry import Point
import json
from coordination import run_traclus
from traclus_dbscan import TrajectoryLineSegmentFactory
import os
import trajectory_partitioning
from traclus_dbscan import TrajectoryLineSegmentFactory, \
    TrajectoryClusterFactory, BestAvailableClusterCandidateIndex
import collections
from representative_trajectory_average_inputs import get_representative_trajectory_average_inputs,\
    DECIMAL_MAX_DIFF_FOR_EQUALITY
from geometry import Point
from representative_line_finding import get_average_vector, get_rotated_line_segment
from pyspark import SparkConf, SparkContext, TaskContext
from pyspark.accumulators import AccumulatorParam
import pandas as pd
from pyrtree import RTree, Rect
from line_segment_averaging import get_representative_line_from_trajectory_line_segments
from datetime import datetime


@click.command()
@click.option(
              '--input-file', '-i',
              help='Input File. Should contain Trajectories and Traclus Parameters.' \
              'See integ_tests/raw_campus_trajectories.txt for an example.',
              required=True)
@click.option(
              '--output-file', '-o',
              help='Output File. Will contain a list of the representative trajectories as Json.',
              required=True)
@click.option(
              '--partitioned-trajectories-output-file-name', '-p',
              help='Optional file to dump the output from the partitioning stage to.')
@click.option(
              '--clusters-output-file-name', '-c', 
              help='Optional file to dump the clusters with their line segments to.')
@click.option(
              '--num-slices', '-n', 
              help='Number of spark partitions to be used for the RDDs.')



def main(input_file, 
         output_file,
         partitioned_trajectories_output_file_name=None,
         clusters_output_file_name=None,
         num_slices=1):

    conf = SparkConf().setAppName('traclus')
    sc = SparkContext(conf=conf)

    sc.addPyFile("./traclus/traclus_impl/geometry.py")
    sc.addPyFile("./traclus/traclus_impl/representative_trajectory_average_inputs.py")
    sc.addPyFile("./traclus/traclus_impl/linked_list.py")
    sc.addPyFile("./traclus/traclus_impl/traclus_dbscan.py")
    sc.addPyFile("./traclus/traclus_impl/distance_functions.py")
    sc.addPyFile("./traclus/traclus_impl/generic_dbscan.py")
    sc.addPyFile("./traclus/traclus_impl/trajectory_partitioning.py")
    sc.addPyFile("./traclus/traclus_impl/mutable_float.py")

    num_slices = int(num_slices)

    # read input file
    parsed_input = None
    with open(get_correct_path_to_file(input_file), 'r') as input_stream:
        parsed_input = json.loads(input_stream.read())
        
    for required_param in ['trajectories',
                           'epsilon',
                           'min_neighbors',
                           'min_num_trajectories_in_cluster',
                            'min_vertical_lines',
                            'min_prev_dist']:
        assert parsed_input[required_param], "missing param: " + str(required_param)   

    # TRACLUS parameters
    epsilon = parsed_input['epsilon']
    min_neighbors = parsed_input['min_neighbors']
    min_num_trajectories_in_cluster = parsed_input['min_num_trajectories_in_cluster']
    min_vertical_lines = parsed_input['min_vertical_lines']
    min_prev_dist = parsed_input['min_prev_dist']

    
    # trajectory data
    trajs = list(map(lambda traj: list(map(lambda pt: Point(**pt), traj)), parsed_input['trajectories']))

    
# start trajectory partitioning phase


    # remove spikes
    trajs = list(map(lambda l: with_spikes_removed(l), trajs))

    cleaned_input = sc.accumulator(list(), ListAccumulator())    

    def clean_input(traj):
        cleaned_traj = []
        if len(traj) > 1:
            prev = traj[0]
            cleaned_traj.append(traj[0])
            for pt in traj[1:]:
                if prev.distance_to(pt) > 0.0:
                    cleaned_traj.append(pt)
                    prev = pt           
            if len(cleaned_traj) > 1:
                cleaned_input.add(cleaned_traj)

    rdd_trajs = sc.parallelize(trajs, numSlices=num_slices)
    rdd_trajs.foreach(lambda l: clean_input(l))
    cleaned_input = flatten_list(cleaned_input.value)
    print('32: cleaned_input')

    # preparing to partition data
    trajectory_line_segment_factory = TrajectoryLineSegmentFactory()
    print('142: get_all_trajectory_line_segments_iterable_from_all_points_iterable')     

    traj_line_segs = sc.accumulator(list(), ListAccumulator()) 
    rdd_cleaned_input = sc.parallelize(cleaned_input, numSlices=num_slices).zipWithIndex()

    def partition_trajectory(point_trajectory_with_index):
        point_trajectory = point_trajectory_with_index[0]
        print('163: get_trajectory_line_segments_from_points_iterable')
        good_indices = trajectory_partitioning.call_partition_trajectory(point_trajectory)
        good_point_iterable = filter_by_indices(good_indices, point_trajectory)

        print('222: consecutive_item_func_iterator_getter')
        out_vals = []
        iterator = iter(good_point_iterable)
        last_item = None
        num_items = 0
        for item in iterator:
            num_items = 1
            last_item = item
            break
        if num_items == 0:
            raise ValueError("iterator doesn't have any values")
            
        for item in iterator:
            num_items += 1
            out_vals.append(trajectory_partitioning.get_line_segment_from_points(last_item, item))
            last_item = item
                
        if num_items < 2:
            raise ValueError("iterator didn't have at least two items")

        line_segs = out_vals

        line_segments = map(lambda line_seg: trajectory_line_segment_factory.new_trajectory_line_seg(line_seg, trajectory_id=point_trajectory_with_index[1]), line_segs)

        temp = 0
        for traj_seg in line_segments:
            traj_line_segs.add(traj_seg)
            temp += 1
        if temp <= 0:
            raise Exception()

    rdd_cleaned_input.foreach(lambda l: partition_trajectory(l))
    traj_line_segs = flatten_list_with_line_seg_index(traj_line_segs.value)
    print("TRAJ_LINE_SEGS_LENGTH: " + str(len(traj_line_segs)))

    partitioned_traj_hook = \
    get_dump_partitioned_trajectories_hook(partitioned_trajectories_output_file_name)

    if partitioned_trajectories_output_file_name != None:
        # dump trajectory partitioning result
        partitioned_traj_hook(traj_line_segs)


# end trajectory partitioning phase
# start clustering phase


    print('66: _dbscan_caller')
    print("CANDIDATES_LENGTH0: " + str(len(traj_line_segs)))

    print('63: dbscan')
    clusters = sc.accumulator(list(), ListAccumulator()) 

    clustered_segs = sc.accumulator(list(), ListAccumulator()) 
    merging_segs = sc.accumulator(list(), ListAccumulator())
    noise_segs = sc.accumulator(list(), ListAccumulator())



    def dbscan_call(partition_id, partition_members, min_neighbors, cluster_factory, cluster_candidates_index):
        print("inside dbscan")
        partition_members = list(partition_members)

        print("======================= dbscan call2")
        print(str(line_seg_index.idx))

        partition_member_ids = list(map(lambda x: x.id, partition_members))

        item_queue = collections.deque()
        
        cluster_id = 0
        for segment in partition_members:
            # get specific cluster candidate that belongs in current partition
            item = cluster_candidates_index.candidates_by_ids[segment.id]

            if not item.is_classified():  
                neighbors = cluster_candidates_index.find_neighbors_of(item)
                if len(neighbors) >= min_neighbors:

                    cur_cluster = cluster_factory.new_cluster(partition_id, cluster_id)
                   
                    cur_cluster.add_member(item)

                    item.assign_to_cluster(cur_cluster)
                    clustered_segs.add([item.id, str(partition_id)+"_"+str(cluster_id), 'a'])
                    
                    for other_item in neighbors:
                        other_item.assign_to_cluster(cur_cluster)
                        cur_cluster.add_member(other_item)

                        clustered_segs.add([other_item.id, str(partition_id)+"_"+str(cluster_id), 'b'])

                        item_queue.append(other_item)

                        if other_item.id not in partition_member_ids:
                            # print("~~~~~~~~~~~"+str(other_item.id))
                            merging_segs.add([other_item.id, str(partition_id)+"_"+str(cluster_id)])
                        
                    print('89: expand_cluster (' + str(len(item_queue)) + ') - ' + str(partition_id))
                    while len(item_queue) > 0:
                        item2 = item_queue.popleft()
                        neighbors2 = cluster_candidates_index.find_neighbors_of(item2)
                        if len(neighbors2) >= min_neighbors:
                            for other_item2 in neighbors2:
                                if not other_item2.is_classified():
                                    item_queue.append(other_item2)
                                if other_item2.is_noise() or not other_item2.is_classified():
                                    other_item2.assign_to_cluster(cur_cluster)

                                    clustered_segs.add([other_item2.id, str(partition_id)+"_"+str( cluster_id), 'c'])
                                    cur_cluster.add_member(other_item2)   

                    # end expand cluster

                    clusters.add(cur_cluster)
                    cluster_id += 1
                else:
                    item.set_as_noise()
                    noise_segs.add(item.id)
                    
        #return clusters

    def dbscan_partition_caller(partition, epsilon, line_seg_index):
        # get partition_id
        task = TaskContext.get()
        partition_id = task.partitionId()
        print("====> partition caller for partition "+str(partition_id))
        segments = list(partition)

        dbscan_call(partition_id, segments, min_neighbors, TrajectoryClusterFactory(), line_seg_index)


    print("======================= R-tree creation ==== ")
    line_seg_index = BestAvailableClusterCandidateIndex(traj_line_segs, epsilon)

    print("preparing rdd for clustering...")
    rdd_cluster_candidates = sc.parallelize(traj_line_segs, numSlices=num_slices)

    print("call dbscan per partition...")
    rdd_cluster_candidates.foreachPartition(lambda rdd_partition: dbscan_partition_caller(rdd_partition, epsilon, line_seg_index))

    # the accumulator values will be flattened and converted to dataframes to find the clusters to be merged
    merging_index = flatten_list(merging_segs.value)
    clustered_index = flatten_list(clustered_segs.value)
    print("MERGING: "+str(merging_index))
    print("CLUSTERED: "+str(clustered_index))
    print("NOISE: "+str(flatten_list(noise_segs.value)))

    clusters = flatten_list_to_dict(clusters.value)

# end clustering phase
# start merging phase

    # # 1. preparation stage (find cluster pairs to be merged)
    # initialize cluster_merges_dictionary;
    
    # for segment in merging_segs:
    #     fake_cluster = segment.cluster
    #     real_cluster = search_clustered_dict(segment.id)
    #     if (!exists(fake_cluster,real_cluster)):
    #         cluster_merges_dictionary.add(fake_cluster -> real_cluster)

    # add in merge list possible duplicates in clustered
    seen = set()
    dupes = []
    no_dupes = []
    for segment in clustered_index:
        if segment[0] in seen:
            dupes.append([segment[0], segment[1]])
        else:
            no_dupes.append([segment[0], segment[1]])
        seen.add(segment[0])

    print("DUPES: " + str(dupes))

    clusters_dict = { no_dupes[i][0]: no_dupes[i][1] for i in range (0, len(no_dupes)) }

    for segment in dupes:
        merging_index.append(segment)

    merge_list = list()
    for segment in merging_index:
        source_cluster_id = segment[1]

        try:
            target_cluster_id = clusters_dict[segment[0]]
        except:
            print("CATCH")
            target_cluster_id = segment[1]

        if (source_cluster_id != target_cluster_id):
            if (([source_cluster_id,target_cluster_id] not in merge_list) and ([target_cluster_id, source_cluster_id] not in merge_list)):
                merge_list.append([source_cluster_id,target_cluster_id])

    print("MERGE LIST: "+str(merge_list))

    # # 2. merging stage
    # for cluster_pair in cluster_merges_dictionary:
    #     if (fake_cluster.members.length == 0):  # already merged
    #         continue;
    #     else:
    #         do:
    #             found = find_merge_target()
    #             if found is null:
    #                 break;
    #             else:
    #                 real_cluster = found
    #         until(real_cluster.members.length > 0);
    #         clusters[real_cluster].merge_cluster(clusters[fake_cluster])

    for merging_couple in merge_list:
        source_cluster_id = merging_couple[0]
        target_cluster_id = merging_couple[1]
        if len(clusters[source_cluster_id].members) > 0 and len(clusters[target_cluster_id].members) > 0:
            print("    BEFORE MERGING: " + source_cluster_id + " (" + str(len(clusters[source_cluster_id].members)) + "), " + target_cluster_id + " (" + str(len(clusters[target_cluster_id].members)) + ")")

            clusters[target_cluster_id].merge_cluster(clusters[source_cluster_id])

            print("    AFTER MERGING: " + source_cluster_id + " (" + str(len(clusters[source_cluster_id].members)) + "), " + target_cluster_id + " (" + str(len(clusters[target_cluster_id].members)) + ")")
        else:
            print("/////////////////////////////// NO MEMBERS FOUND. /////////////////////////")

# end merging phase

    # print merged (final) clusters
    clusters_hook = \
    get_dump_clusters_hook(clusters_output_file_name)

    if clusters_output_file_name != None:
        # dump trajectory partitioning result
        clusters_hook(clusters)

# start representative generation phase

    print(datetime.now().strftime("%H:%M:%S"))
    
    rep_clusters = []
    for traj_cluster in clusters:
        if clusters[traj_cluster].num_trajectories_contained() >= min_num_trajectories_in_cluster:

            line_segments = clusters[traj_cluster].get_trajectory_line_segments()
            line_segments_list = list(map(lambda l: {"start_x":l.line_segment.start.x, "start_y":l.line_segment.start.y, "end_x":l.line_segment.end.x, "end_y":l.line_segment.end.y} , line_segments))

            representative = get_representative_line_from_trajectory_line_segments(trajectory_line_segments=line_segments, min_vertical_lines=min_vertical_lines, min_prev_dist=min_prev_dist)
            representative = list(map(lambda pt: pt.as_dict(), representative))

            rep_clusters.append({"cluster_segments":line_segments_list, "representative":representative})

    print(str(len(rep_clusters)) + " representatives generated.")
    print(datetime.now().strftime("%H:%M:%S"))
           
# end representative generation phase

    # write output to file
    dict_result = list(map(lambda traj: traj, rep_clusters))
    
    with open(get_correct_path_to_file(output_file), 'w') as output_stream:
        output_stream.write(json.dumps(dict_result))









# trajectory partitioning helpers
def with_spikes_removed(trajectory):
    if len(trajectory) <= 2:
        return trajectory[:]
        
    spikes_removed = []
    spikes_removed.append(trajectory[0])
    cur_index = 1
    while cur_index < len(trajectory) - 1:
        if trajectory[cur_index - 1].distance_to(trajectory[cur_index + 1]) > 0.0:
            spikes_removed.append(trajectory[cur_index])
        cur_index += 1
        
    spikes_removed.append(trajectory[cur_index])
    return spikes_removed


# flatten list
def flatten_list(input_list):
    flat_list = []
    for list_item in input_list:
        for list_item_item in list_item:
            flat_list.append(list_item_item)

    return flat_list

def flatten_list_with_line_seg_index(input_list):
    line_seg_id = 0
    flat_list = []
    for list_item in input_list:
        for list_item_item in list_item:
            list_item_item.id = line_seg_id
            flat_list.append(list_item_item)
            line_seg_id += 1
            
    return flat_list

def flatten_list_to_dict(input_list):
    output_dictionary = {}
    for partition_clusters in input_list:
        for cluster in partition_clusters:
            output_dictionary[str(cluster.partitionId)+"_"+str(cluster.clusterId)] = cluster

    return output_dictionary

def filter_by_indices(good_indices, vals):
    print('180: filter_by_indices')
    vals_iter = iter(vals)
    good_indices_iter = iter(good_indices)
    out_vals = []
    
    num_vals = 0
    for i in good_indices_iter:
        if i != 0:
            raise ValueError("the first index should be 0, but it was " + str(i))
        else:
            for item in vals_iter:
                out_vals.append(item)
                break
            num_vals = 1
            break
            
    max_good_index = 0
    vals_cur_index = 1
    for i in good_indices_iter:
        max_good_index = i
        for item in vals_iter:
            num_vals += 1
            if vals_cur_index == i:
                vals_cur_index += 1
                out_vals.append(item)
                break
            else:
                vals_cur_index += 1
                
    for i in vals_iter:
        num_vals += 1
                
    if num_vals < 2:
        raise ValueError("list passed in is too short")
    if max_good_index != num_vals - 1:
        raise ValueError("last index is " + str(max_good_index) + \
                         " but there were " + str(num_vals) + " vals")
    return out_vals
        

# dump-to-file functions
def get_dump_partitioned_trajectories_hook(file_name):
    if not file_name:
        return None
    
    def func(partitioned_stage_output):
        dict_trajs = list(map(lambda traj_line_seg: traj_line_seg.line_segment.as_dict(), 
                         partitioned_stage_output))
        with open(get_correct_path_to_file(file_name), 'w') as output:
            output.write(json.dumps(dict_trajs))
    return func    

def get_dump_clusters_hook(file_name):
    if not file_name:
        return None
    
    def func(clusters):
        all_cluster_line_segs = []
        clusters_with_members_cnt = 0
        clusters_merged = 0

        for clust in clusters:
            if len(clusters[clust].members) > 0:
                clusters_with_members_cnt += 1
            else:
                clusters_merged += 1

            print("cluster data: " + str(clusters[clust].partitionId) + ", " + str(clusters[clust].clusterId) +", "+str(len(clusters[clust].members)))

            line_segs = clusters[clust].get_trajectory_line_segments()
            dict_output = list(map(lambda traj_line_seg: traj_line_seg.line_segment.as_dict(), 
                              line_segs))
            all_cluster_line_segs.append(dict_output)
            
        print("NUMBER OF NON-ZERO CLUSTERS: " + str(clusters_with_members_cnt))
        print("CLUSTERS MERGED: " + str(clusters_merged))

        with open(get_correct_path_to_file(file_name), 'w') as output:
            output.write(json.dumps(all_cluster_line_segs))
    return func
            
def get_correct_path_to_file(file_name):
    return file_name


# custom accumulators
# list accumulator
class ListAccumulator(AccumulatorParam):
    def zero(self, init_value=list()):
        return list()

    def addInPlace(self, l1:list, l2:list):
        l1.append(l2)
        return l1


if __name__ == '__main__':
    main()

# Distributed TRACLUS implementation using Apache Spark

Distributed implementation of TRACLUS based on [apolcyn's single-thread implementation in Python](https://github.com/apolcyn/traclus_impl).

### Converters
* **```csv2json.py```**
  Takes as input a CSV file containing the vessel trajectory points and returns a JSON file for use in the traclus_impl scripts.
  
  Example: ```python3 csv2json.py -i ~/Downloads/AIS_2021_02_01.csv -o ../datasets/ais_50_traj.csv -k 50```<br />
  or ```python3 csv2json.py --ifile ~/Downloads/AIS_2021_02_01.csv --ofile ../datasets/ais_50_traj.csv -trajectories_to_keep 50```

  **Parameters:** <br />
    &nbsp;&nbsp;"-h", "--help": help<br />
    &nbsp;&nbsp;"-i", "--ifile": input file<br />
    &nbsp;&nbsp;"-o", "--ofile": output file<br />
    &nbsp;&nbsp;"-s", "--csv_sep": CSV separator character (default: ',')<br />
    &nbsp;&nbsp;"-k", "--trajectories_to_keep": number of trajectories to keep from the dataset<br />
    &nbsp;&nbsp;&nbsp;&nbsp;**Dataset column parameters:**<br />
    &nbsp;&nbsp;"-r", "--tid_col": trajectory_id, vessel identifier column (default: MMSI)<br />
    &nbsp;&nbsp;"-x", "--lon_col": longitude column (default: LON)<br />
    &nbsp;&nbsp;"-y", "--lat_col": latitude column (default: LAT)<br />
    &nbsp;&nbsp;"-t", "--tim_col": timestamp column (default: BaseDateTime)<br />
    &nbsp;&nbsp;&nbsp;&nbsp;**TRACLUS parameters:**<br />
    &nbsp;&nbsp;"-e", "--epsilon": neighborhood radius (default: 0.016)<br />
    &nbsp;&nbsp;"-n", "--min_neighbors": min number of neighbors in neighborhood, to consider a line segment as "core line segment" (default: 2)<br />
    &nbsp;&nbsp;"-c", "--min_num_trajectories_in_cluster": minimum number of distinct trajectories to consider a cluster as valid (default: 3)<br />
    &nbsp;&nbsp;"-v", "--min_vertical_lines": used for representatives calculation (default: 2)<br />
    &nbsp;&nbsp;"-d", "--min_prev_dist": used for representatives calculation (default: 0.0002)<br />

* **```set_parameters.py```**
  Takes as input a JSON dataset file and changes its parameters based on the command line arguments given. Can save the results in the same or another file.
  
  Example: ```python3 set_parameters.py -i input_file.json -o output_file.json -e 0.016 -n 3```<br />
  or ```python3 set_parameters.py --ifile input_file.json --ofile output_file.json --epsilon 0.016 --min_neighbors 3```

  **Parameters:** <br />
    &nbsp;&nbsp;"-h", "--help": help<br />
    &nbsp;&nbsp;"-i", "--ifile": input file<br />
    &nbsp;&nbsp;"-o", "--ofile": output file<br />
    &nbsp;&nbsp;&nbsp;&nbsp;**TRACLUS parameters:**<br />
    &nbsp;&nbsp;"-e", "--epsilon": neighborhood radius (default: 0.016)<br />
    &nbsp;&nbsp;"-n", "--min_neighbors": min number of neighbors in neighborhood, to consider a line segment as "core line segment" (default: 2)<br />
    &nbsp;&nbsp;"-c", "--min_num_trajectories_in_cluster": minimum number of distinct trajectories to consider a cluster as valid (default: 3)<br />
    &nbsp;&nbsp;"-v", "--min_vertical_lines": used for representatives calculation (default: 2)<br />
    &nbsp;&nbsp;"-d", "--min_prev_dist": used for representatives calculation (default: 0.0002)<br />

* **```json2geojson.py```**
  Takes as input a JSON file (dataset or result file) and returns geoJSON geometries of the spatial entities contained in the JSON file. Works for datasets, dataset partitions, spatial dataset partitioning rectangles, clusters, representatives and clustering significance results (cluster with related representative).

  Example: ```python3 json2geojson.py -i input_file.json -o output_file.json -d dataset```<br />
  or ```python3 json2geojson.py -ifile input_file.json -ofile output_file.json -data_type dataset```

  **Parameters:** <br />
    &nbsp;&nbsp;"-h", "--help": help<br />
    &nbsp;&nbsp;"-i", "--ifile": input file<br />
    &nbsp;&nbsp;"-o", "--ofile": output file<br />
    &nbsp;&nbsp;"-d", "--data_type": type of input data (possible values: dataset|results|partitions|clusters|representatives|partitioning_rectangles|significance)<br />
    &nbsp;&nbsp;"-t", "--trajectories_element_name": name of the JSON element containing the trajectories (default: trajectories)<br />
  
* **```stats_timestamps.py```**
  Takes as input a JSON file containing the timestamps of each execution phase of the algorithm and prints at the screen the times in seconds for each execution interval. Works for the single-thread, random partitioning and spatial partitioning implementations.

  Example: ```python3 stats_timestamps.py -i input_file.json -d spatial```<br />
  or ```python3 stats_timestamps.py -ifile input_file.json -data_type spatial```

  **Parameters:** <br />
    &nbsp;&nbsp;"-h", "--help": help<br />
    &nbsp;&nbsp;"-i", "--ifile": input file<br />
    &nbsp;&nbsp;"-d", "--data_type": type of input data (possible values: single|spatial|random, depending on the TRACLUS implementation that generated the timestamps)


### Single-thread implementation
Example: ```python3 traclus/traclus_impl/main.py -i path_to_dataset_file.json -o path_to_results_file.json -p path_to_patitioning_results_file.json -c path_to_clustering_results_file.json```

**Parameters:**<br />
  &nbsp;&nbsp;"-i", "--input-file": input file, contains trajectories and TRACLUS parameters<br />
  &nbsp;&nbsp;"-o", "--output-file": output file<br />
  &nbsp;&nbsp;"-p", "--partitioned-trajectories-output-file-name": optional file to dump the output from the partitioning phase<br />
  &nbsp;&nbsp;"-c", "--clusters-output-file-name": optional file to dump the output from the line segment clustering phase<br />


### Distributed ```d-TRACLUS-*``` implementations
* Spark distributed with random partitioning (executed from branch: pyspark_random_partitioning)
  Performs the distributed TRACLUS implementation **d-TraClus-R**, using random partitioning to split the line segments dataset.<br />
  Example: ```spark-submit --master local[*] traclus/traclus_impl/main.py -i path_to_dataset_file.json -o path_to_results_file.json -n 16```<br />

* Spark distributed with spatial partitioning (executed from branch: pyspark_spatial_partitioning)
  Performs the distributed TRACLUS implementation **d-TraClus-S**, using spatial partitioning to split the line segments dataset.<br />
  Example: ```spark-submit --master local[*] traclus/traclus_impl/main.py -i path_to_dataset_file.json -o path_to_results_file.json -n 16```<br />

  **Parameters:**<br />
    &nbsp;&nbsp;"-i", "--input-file": input file, contains trajectories and TRACLUS parameters<br />
    &nbsp;&nbsp;"-o", "--output-file": output file<br />
    &nbsp;&nbsp;"-p", "--partitioned-trajectories-output-file-name": optional file to dump the output from the partitioning phase<br />
    &nbsp;&nbsp;"-c", "--clusters-output-file-name": optional file to dump the output from the line segment clustering phase<br />
    &nbsp;&nbsp;"-n", "--num-slices": number of spark partitions to be used for the RDDs<br />


### Clustering significance testing
* **```cluster_significance.py```**
  Performs statistical significance tests on the generated clusters, based on their representatives. The tests performed are the Z-test and Kolmogorov-Smirnov test.<br />
  Example: ```python3 significance/cluster_significance.py -i path_to_dataset_file.json -o path_to_significance_testing_results_file.json```<br />

  **Parameters:**<br />
    &nbsp;&nbsp;"-i", "--input-file": input file<br />
    &nbsp;&nbsp;"-o", "--output-file": output file, contains statistical significance testing results<br />

import click
import json
from frechetdist import frdist
import random
import statistics
import math
from scipy import stats

@click.command()
@click.option(
            '--input-file', '-i',
            help='Input File. Should contain cluster members and their related representatives in a JSON array.',
            required=True)
@click.option(
            '--output-file', '-o',
            help='Output File. Will contain a list of the representative trajectories as Json.',
            required=True)


def main(input_file, output_file):
    # read input file (JSON format)
    rep_clusters = None
    with open(input_file, 'r') as input_stream:
        rep_clusters = json.loads(input_stream.read())

# start statistical significance testing phase

    # STATISTICAL SIGNIFICANCE
    # Find bounding box of representative
    # Generate random (trajectories) LINE SEGMENTS inside the bounding box of the representative 
    #     (their length may should be equal to the mean trajectory of the cluster members)
    # Calculate real cluster similarity to representative
    #     per cluster member (line segment) to representative trajectory
    # Calculate random trajectories similarity to representative
    # Compare the two distances

    cluster_cnt = 0
    for rep_cluster in rep_clusters:
        cluster_cnt += 1
        repr_traj = rep_cluster["representative"]
        repr_min_x, repr_min_y, repr_max_x, repr_max_y = representative_mbb(repr_traj)
        print("Cluster #" + str(cluster_cnt) + ":\n  Members: " + str(len(rep_cluster["cluster_segments"])) + \
            "\n  Representative points: " + str(len(repr_traj)))

        print("Calculating real distances...")
        real_cluster_distances = calculate_real_distances(repr_traj, rep_cluster["cluster_segments"])

        print("Calculating fake distances...")
        fake_cluster_distances = calculate_fake_distances(repr_traj, repr_min_x, repr_min_y, \
            repr_max_x, repr_max_y, rep_cluster["cluster_segments"])
    
        # z-test
        print("Performing Z-test for statistical significance...")
        significance_test_result = z_test(real_cluster_distances, fake_cluster_distances)
        rep_cluster["clustering_significance"] = significance_test_result
        print("  Result: " + significance_test_result["ztest_significance_result"] + \
            "\n  Z-test score: " + str(significance_test_result["ztest_score"]))

        # kolmogorov-smirnov test
        print("Performing Kolmogorov-Smirnov statistical significance test...")
        significance_test_result = stats.kstest(real_cluster_distances, fake_cluster_distances)
        rep_cluster["clustering_significance"]["ks_test_statistic"] = significance_test_result.statistic
        rep_cluster["clustering_significance"]["ks_test_pvalue"] = significance_test_result.pvalue
        print("  Result statistic: " + str(significance_test_result.statistic) + \
            "\n  p-value: " + str(significance_test_result.pvalue) + "\n")

# end statistical significance testing phase

# write output to file 
    with open(output_file, 'w') as output_stream:
        output_stream.write(json.dumps(rep_clusters))




def trajectoryLineSegment_to_list(segment):
    output = []
    output.append([segment["start_x"], segment["start_y"]])
    output.append([segment["end_x"], segment["end_y"]])
    return output


def representativePoints_to_list(start_point, end_point):
    output = []
    output.append([start_point["x"], start_point["y"]])
    output.append([end_point["x"], end_point["y"]])
    return output


def representative_mbb(repr_traj):
    repr_min_x = 190
    repr_min_y = 190
    repr_max_x = -190
    repr_max_y = -190

    for point in repr_traj:
        if point["x"] < repr_min_x:
            repr_min_x = point["x"]
        elif point["x"] > repr_max_x:
            repr_max_x = point["x"]

        if point["y"] < repr_min_y:
            repr_min_y = point["y"]
        elif point["y"] > repr_max_y:
            repr_max_y = point["y"]

    #print("min_x_y: ("+str(repr_min_x)+", "+str(repr_min_y)+")   max_x_y: ("+str(repr_max_x)+", "+str(repr_max_y)+")")
    return repr_min_x, repr_min_y, repr_max_x, repr_max_y


def calculate_real_distances(repr_traj, cluster_segments):
    real_cluster_distances = []
    
    for cluster_member in cluster_segments:
        cluster_seg = trajectoryLineSegment_to_list(cluster_member)

        min_dist = 0
        set_min_dist = False

        for point in range(len(repr_traj)-1):
            repr_seg = representativePoints_to_list(repr_traj[point], repr_traj[point+1])
            dist = frdist(cluster_seg, repr_seg)
            if not set_min_dist: 
                min_dist = dist
                set_min_dist = True
            elif dist < min_dist:
                min_dist = dist

        real_cluster_distances.append(min_dist)

    return real_cluster_distances


def calculate_fake_distances(repr_traj, repr_min_x, repr_min_y, repr_max_x, repr_max_y, cluster_segments):
    fake_cluster_distances = []

    for i in range(len(cluster_segments)):
        start_point_x = random.uniform(repr_min_x, repr_max_x)
        start_point_y = random.uniform(repr_min_y, repr_max_y)
        end_point_x = random.uniform(repr_min_x, repr_max_x)
        end_point_y = random.uniform(repr_min_y, repr_max_y)

        fake_seg = [[start_point_x, start_point_y],[end_point_x, end_point_y]]

        # calculate distances
        min_dist_fake = 0
        set_min_dist_fake = False

        for point in range(len(repr_traj)-1):
            repr_seg = representativePoints_to_list(repr_traj[point], repr_traj[point+1])
            dist_fake = frdist(fake_seg, repr_seg)
            if not set_min_dist_fake: 
                min_dist_fake = dist_fake
                set_min_dist_fake = True
            elif dist_fake < min_dist_fake:
                min_dist_fake = dist_fake

        fake_cluster_distances.append(min_dist_fake)

    return fake_cluster_distances


def z_test(real_cluster_distances, fake_cluster_distances):
    if (len(real_cluster_distances) > 0) and (len(fake_cluster_distances) > 0):
        real_mean = statistics.mean(real_cluster_distances)
        fake_mean = statistics.mean(fake_cluster_distances)
        #print("real mean: "+str(real_mean))
        #print("fake mean: "+str(fake_mean))

        real_stdev = statistics.stdev(real_cluster_distances)
        fake_stdev = statistics.stdev(fake_cluster_distances)
        #print("real stdev: "+str(real_stdev))
        #print("fake stdev: "+str(fake_stdev))

        real_sigma = real_stdev / math.sqrt(len(real_cluster_distances))
        fake_sigma = fake_stdev / math.sqrt(len(fake_cluster_distances))
        #print("real sigma: "+str(real_sigma))
        #print("fake sigma: "+str(fake_sigma))

        z = (fake_mean - real_mean) / (math.sqrt((real_sigma ** 2) + (fake_sigma ** 2)))
        # print("Z-test result: " + str(z))

        if z < 2:
            significance_result = "Clustering of no significance."
        elif z >= 2.0 and z < 2.5:
            significance_result = "Clustering of marginal significance."
        elif z >= 2.5 and z < 3.0:
            significance_result = "Significant clustering."
        else:
            significance_result = "Highly significant clustering."

    else:
        significance_result = "Cannot calculate significance for zero-length distributions."

    return {"ztest_score": z, "ztest_significance_result": significance_result}


if __name__ == '__main__':
    main()

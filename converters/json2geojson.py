#!/opt/anaconda/bin/python

from os import linesep
import sys
import getopt
import json


global_inputfile = "dataset.json"
global_outputfile = "geo_dataset.json"
global_datatype = "dataset"
global_trajectorieselementname = "trajectories"


class FeatureCollection(object):
    def __init__(self):
        self.type = "FeatureCollection"
        self.features = []

    def to_dict(self):
        return {"type":self.type,"features":list(map(lambda ft: ft.to_dict(), self.features))}


class Feature(object):
    def __init__(self):
        self.type = "Feature"
        self.geometry = None
        self.properties = FeatureProperties()

    def __init__(self, geometry):
        self.type = "Feature"
        self.geometry = geometry
        self.properties = FeatureProperties()

    def to_dict(self):
        return {"type":self.type,"geometry": self.geometry.to_dict(), "properties": self.properties.to_dict()}


class FeatureProperties(object):
    def __init__(self):
        self.property0 = ""
        self.property1 = ""
        self.property2 = ""

    def to_dict(self):
        return {"prop0": self.property0, "prop1": self.property1, "prop2": self.property2}


class LineString(object):
    def __init__(self):
        self.type = "LineString"
        self.coordinates = []

    def __init__(self, coordinates):
        self.type = "LineString"
        self.coordinates = coordinates

    def to_dict(self):
        return {"type":self.type, "coordinates": self.coordinates}


class Polygon(object):
    def __init__(self):
        self.type = "Polygon"
        self.coordinates = []

    def __init__(self, coordinates):
        self.type = "Polygon"
        self.coordinates = coordinates

    def to_dict(self):
        return {"type":self.type, "coordinates": self.coordinates}


class MultiLineString(object):
    def __init__(self):
        self.type = "MultiLineString"
        self.coordinates = []

    def __init__(self, coordinates):
        self.type = "MultiLineString"
        self.coordinates = coordinates

    def to_dict(self):
        return {"type":self.type, "coordinates": self.coordinates}


def main(argv):
    global global_inputfile
    global global_outputfile
    global global_datatype
    global global_trajectorieselementname

    help_message = ""

    try:
        opts, args = getopt.getopt(argv,"hi:o:d:t:",["ifile=","ofile=","data_type=","trajectories_element_name="])
    except getopt.GetoptError:  
        print("\nError in command line arguments.\n" + help_message)
        sys.exit(2)

    # read script input arguments
    for opt, arg in opts:
        # file parameters
        if opt in ("-h", "--help"):
            print(help_message)
            sys.exit()
        elif opt in ("-i", "--ifile"):
            global_inputfile = arg
        elif opt in ("-o", "--ofile"):
            global_outputfile = arg
        elif opt in ("-d", "--data_type"):
            global_datatype = arg
        elif opt in ("-t", "--trajectories_element_name"):
            global_trajectorieselementname = arg
        
    try:
        with open(global_inputfile, 'r') as input_stream:
            parsed_input = json.loads(input_stream.read())
    except:
        print("\nError while reading file.\n", sys.exc_info()[0])
        sys.exit(2)

    trajectories = []
    feature_collection = FeatureCollection()

    if global_datatype == "dataset":
        trajectories = parsed_input[global_trajectorieselementname]
    else:
        trajectories = parsed_input
    
    if global_datatype == "dataset" or global_datatype == "results":
        for trajectory in trajectories:
            traj_points = []

            for point in trajectory:
                traj_points.append([point["x"], point["y"]])
            
            feature_collection.features.append(Feature(LineString(traj_points)))

    elif global_datatype == "partitions":
        for trajectory_segment in trajectories:
            # {"start": {"x": 37.04209, "y": -88.53539}, "end": {"x": 37.04208, "y": -88.53538}}
            segment_points = []
            #import code; code.interact(local=dict(globals(), **locals()))
            segment_points.append([trajectory_segment["start"]["x"], trajectory_segment["start"]["y"]])
            segment_points.append([trajectory_segment["end"]["x"], trajectory_segment["end"]["y"]])

            feature_collection.features.append(Feature(LineString(segment_points)))

    elif global_datatype == "clusters":
        for cluster in trajectories:
            cluster_member_coordinates = []
            # [{"start": {"x": 37.04209, "y": -88.53539}, "end": {"x": 37.04208, "y": -88.53538}}, ... ]
            for cluster_member in cluster:
                cluster_member_points = []
                # {"start": {"x": 37.04209, "y": -88.53539}, "end": {"x": 37.04208, "y": -88.53538}}
                cluster_member_points.append([cluster_member["start"]["x"], cluster_member["start"]["y"]])
                cluster_member_points.append([cluster_member["end"]["x"], cluster_member["end"]["y"]])

                cluster_member_coordinates.append(cluster_member_points)


            feature_collection.features.append(Feature(MultiLineString(cluster_member_coordinates)))

    elif global_datatype == "representatives":
        for representative in trajectories:
            representative_coordinates = []
            # [{"cluster_segments": [{"start_x": -90.40058, "start_y": 29.97754, "end_x": -90.40058, "end_y": 29.977520000000002}, ... }, ... ]
            for representative_segment in representative["cluster_segments"]:
                representative_points = []
                # {"start_x": -90.40058, "start_y": 29.97754, "end_x": -90.40058, "end_y": 29.977520000000002}
                representative_points.append([representative_segment["start_x"], representative_segment["start_y"]])
                representative_points.append([representative_segment["end_x"], representative_segment["end_y"]])

                representative_coordinates.append(representative_points)


            feature_collection.features.append(Feature(MultiLineString(representative_coordinates)))

    elif global_datatype == "partitioning_rectangles":
        for rect in trajectories:
            rectangle_coordinates = []
            rectangle_coordinates.append([rect["x"], rect["y"]])
            rectangle_coordinates.append([rect["x"], rect["yy"]])
            rectangle_coordinates.append([rect["xx"], rect["yy"]])
            rectangle_coordinates.append([rect["xx"], rect["y"]])
            rectangle_coordinates.append([rect["x"], rect["y"]])

            feature_collection.features.append(Feature(Polygon([rectangle_coordinates])))

    elif global_datatype == "significance":
        cnt_cluster = 0
        for cluster_result in trajectories:
            #cluster_member_coordinates = []
            # [{"cluster_segments": [{"start_x": -90.40058, "start_y": 29.97754, "end_x": -90.40058, "end_y": 29.977520000000002}, ... }, ... ]
            for cluster_member in cluster_result["cluster_segments"]:
                cluster_member_points = []
                # {"start_x": -82.56085999999999, "start_y": 27.69371, "end_x": -82.5683, "end_y": 27.68865}
                cluster_member_points.append([cluster_member["start_x"], cluster_member["start_y"]])
                cluster_member_points.append([cluster_member["end_x"], cluster_member["end_y"]])

                #cluster_member_coordinates.append(cluster_member_points)

                geo_feature = Feature(LineString(cluster_member_points))
                geo_feature.properties.property0 = "cluster" + str(cnt_cluster)       
                geo_feature.properties.property1 = "cluster_member"
                feature_collection.features.append(geo_feature)

            # feature_collection.features.append(Feature(MultiLineString(cluster_member_coordinates)))


            representative_coordinates = []
            # "representative": [{"x": -85.81730999999999, "y": 27.586729999999996}, ... ]
            for representative_segment in cluster_result["representative"]:
                representative_coordinates.append([representative_segment["x"], representative_segment["y"]])
                #representative_points.append([representative_segment["end_x"], representative_segment["end_y"]])

            geo_feature = Feature(LineString(representative_coordinates))
            geo_feature.properties.property0 = "cluster" + str(cnt_cluster)       
            geo_feature.properties.property1 = "cluster_representative"
            geo_feature.properties.property2 = str(cluster_result["clustering_significance"])

            feature_collection.features.append(geo_feature)

            cnt_cluster += 1
    

    json_output = json.dumps(feature_collection.to_dict())

    f = open(global_outputfile, "w")
    f.write(json_output)
    f.close()

    #import code; code.interact(local=dict(globals(), **locals()))


if __name__ == "__main__":
    main(sys.argv[1:])

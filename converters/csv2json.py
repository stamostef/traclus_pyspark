#!/opt/anaconda/bin/python

import sys
import pandas as pd
import json
import read_clean_data as cd


class TraclusInputFileObject(object):
    def __init__(self, epsilon, min_neighbors, min_num_trajectories_in_cluster, min_vertical_lines, min_prev_dist):
        self.epsilon = epsilon
        self.min_neighbors = min_neighbors
        self.min_num_trajectories_in_cluster = min_num_trajectories_in_cluster
        self.min_vertical_lines = min_vertical_lines
        self.min_prev_dist = min_prev_dist
        self.trajectories = []


def main(argv):
    # import dataset
    raw_data = cd.import_dataset_from_file(argv)

    # clean dataset
    clean_data = cd.clean_dataset(raw_data, cd.global_clean_stationary_points, cd.global_resolvesubtrajectories)

    # initialize object
    input_object = TraclusInputFileObject(cd.global_epsilon, cd.global_min_neighbors, cd.global_min_num_trajectories_in_cluster, cd.global_min_vertical_lines, cd.global_min_prev_dist)

    # add trajectories in input_object
    for traj in pd.unique(clean_data[cd.global_column_name_traj_id]):

        traj_locations = clean_data.loc[clean_data[cd.global_column_name_traj_id] == traj].values.tolist()
        input_object.trajectories.append(list(map(lambda location: {"x": location[2], "y": location[1]}, traj_locations)))


    # write input_object to file as JSON
    json_obj = json.dumps(input_object.__dict__)

    f = open(cd.global_outputfile, "w")
    f.write(json_obj)
    f.close()


if __name__ == "__main__":
    main(sys.argv[1:])

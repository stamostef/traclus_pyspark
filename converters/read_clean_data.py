import sys
import getopt
import pandas as pd
import math

# global variables
# file parameters
global_inputfile = "dataset.csv"
global_outputfile = "dataset.json"
global_csv_separator = ","

# dataset column parameters
global_column_name_traj_id = "MMSI"
global_column_name_lon = "LON"
global_column_name_lat = "LAT"
global_column_name_time = "BaseDateTime"

# TRACLUS parameters
global_epsilon = 0.016
global_min_neighbors = 2
global_min_num_trajectories_in_cluster = 3
global_min_vertical_lines = 2
global_min_prev_dist = 0.0002

global_clean_stationary_points = False
global_trajectories_to_keep = -1    # keeps all rows
global_resolvesubtrajectories = False
global_trajectoryresolutionthreshold = 0.5  # in degrees


def import_dataset_from_file(argv):    
    global global_inputfile
    global global_outputfile
    global global_csv_separator
    global global_column_name_traj_id
    global global_column_name_lon
    global global_column_name_lat
    global global_column_name_time
    global global_epsilon
    global global_min_neighbors
    global global_min_num_trajectories_in_cluster
    global global_min_vertical_lines
    global global_min_prev_dist
    # global global_clean_stationary_points
    global global_trajectories_to_keep
    # global global_resolvesubtrajectories

    help_message = "Available arguments (parentheses contain default values):\n        -i --ifile    Input_file (dataset.csv)\n        -o --ofile    Output file (dataset.json)>\n        -s  --csv_sep    Csv separator (,)\n        -r --tid_col    Trajectory id column (MMSI)\n        -x --lon_col    Longitude column (LON)\n        -y --lat_col    Latitude column (LAT)\n        -t --tim_col    Time column (BaseDateTime)\n        -e --epsilon    TRACLUS epsilon parameter (0.00016)\n        -n --min_neighbors    TRACLUS neighbors parameter (2)\n        -c --min_num_trajectories_in_cluster    TRACLUS min number of trajectories in cluster parameter (3)\n        -v --min_vertical_lines    TRACLUS min number of vertical lines parameter (2)\n        -d --min_prev_dist    TRACLUS min previous distance parameter (0.0002)\n        -k --trajectories_to_keep    Trajectories to keep for the JSON dataset file (-1)"

    try:
        opts, args = getopt.getopt(argv,"hi:o:s:r:x:y:t:e:n:c:v:d:p:k:u:",["ifile=","ofile=","csv_sep=","tid_col=","lon_col=","lat_col=","tim_col=","epsilon=","min_neighbors=","min_num_trajectories_in_cluster=","min_vertical_lines=","min_prev_dist=","clean_stationary_points=","trajectories_to_keep=","resolve_subtrajectories="])
    except getopt.GetoptError:  
        print("\nError in command line arguments.\n" + help_message)
        sys.exit(2)

    # read script input arguments
    for opt, arg in opts:
        # file parameters
        if opt in ("-h", "--help"):
            print(help_message)
            sys.exit()
        elif opt in ("-i", "--ifile"):
            global_inputfile = arg
        elif opt in ("-o", "--ofile"):
            global_outputfile = arg
        elif opt in ("-s", "--csv_sep"):
            global_csv_separator = arg

        # dataset column parameters
        elif opt in ("-r", "--tid_col"):
            global_column_name_traj_id = arg
        elif opt in ("-x", "--lon_col"):
            global_column_name_lon = arg
        elif opt in ("-y", "--lat_col"):
            global_column_name_lat = arg
        elif opt in ("-t", "--tim_col"):
            global_column_name_time = arg

        # TRACLUS parameters
        elif opt in ("-e", "--epsilon"):
            global_epsilon = float(arg)
        elif opt in ("-n", "--min_neighbors"):
            global_min_neighbors = int(arg)
        elif opt in ("-c", "--min_num_trajectories_in_cluster"):
            global_min_num_trajectories_in_cluster = int(arg)
        elif opt in ("-v", "--min_vertical_lines"):
            global_min_vertical_lines = int(arg)
        elif opt in ("-d", "--min_prev_dist"):
            global_min_prev_dist = float(arg)

        # elif opt in ("-p", "--clean_stationary_points"):
        #     if arg == 'True':
        #         global_clean_stationary_points = True
        elif opt in ("-k", "--trajectories_to_keep"):
            global_trajectories_to_keep = int(arg)
        # elif opt in ("-u", "--resolve_subtrajectories"):
        #     if arg == 'True':
        #         global_resolvesubtrajectories = True

    # read csv file containing data
    print("Reading input file...\nCSV separator: {}\nColumn names:     trajectory_id: {}\n                  longitude: {}\n                  latitude: {}\n                  time: {}".format(global_csv_separator, global_column_name_traj_id, global_column_name_lon, global_column_name_lat, global_column_name_time))

    try:
        data = pd.read_csv(global_inputfile, sep=global_csv_separator)

        # check existence of column names in dataframe
        for col_name in [global_column_name_traj_id, global_column_name_lon, global_column_name_lat, global_column_name_time]:
            if col_name not in data:
                print(col_name + " column not found in the dataset.")
                raise

        print("Dataset imported successfully.")

        # return the necessary for the analysis dataset columns
        return data[[global_column_name_traj_id, global_column_name_lat, global_column_name_lon, global_column_name_time]]

    except:
        print("Error in input file format or file not found.", sys.exc_info()[0])
        exit(2)


def clean_dataset(data, clean_stationary_points, resolve_subtrajectories):
    # sort data by trajectory and time, remove time column after sorting
    print("Sorting dataset by trajectory...")
    clean = sort_data_by_trajectory(data, global_column_name_traj_id, global_column_name_time)
    #import code; code.interact(local=dict(globals(), **locals()))

    # remove stationary points
    print("Removing stationary points...")
    clean = remove_stationary_points(clean)

    # resolve id conflicts of trajectories moving in different locations
    print("Resolving conflicting trajectory IDs...")
    clean_df = resolve_trajectory_id_conflicts(clean)

    # remove time column, it will not be used in the analysis
    clean = clean_df.drop(global_column_name_time, axis='columns')

    return clean


def convert_list_to_dataframe(clean_data_list):
    arr = []
    for traj in range(0, len(clean_data_list)):
        for point in range(0, len(clean_data_list[traj])):
            arr.append(clean_data_list[traj][point])

    df = pd.DataFrame(arr, columns=[global_column_name_traj_id, global_column_name_lat, global_column_name_lon, global_column_name_time])

    return df


def sort_data_by_trajectory(data, trajid_col, time_col):
    out_data = []
    for traj_id in pd.unique(data[trajid_col]):
        sorted_traj = data.loc[data[trajid_col] == traj_id].sort_values(by=[time_col]).values.tolist()
        out_data.append(sorted_traj)

    return out_data


def remove_stationary_points(sorted_data_list):
    global global_trajectories_to_keep

    if global_trajectories_to_keep == -1:
        keep_all_trajectories = True
    else:
        keep_all_trajectories = False

    traj_cnt = 0
    cleaned_input = []
    for traj in sorted_data_list:
        traj_cnt += 1

        if not keep_all_trajectories:
            if traj_cnt > global_trajectories_to_keep:
                break

        cleaned_traj = []
        if len(traj) > 1:
            prev = traj[0]
            cleaned_traj.append(traj[0])
            for pt in traj[1:]:
                # if the points' LAT or LON is different (not the same point), append to list
                if prev[1] != pt[1] or prev[2] != pt[2]:
                    cleaned_traj.append(pt)
                    prev = pt
            if len(cleaned_traj) > 1:
                cleaned_input.append(cleaned_traj)

    return cleaned_input


def resolve_trajectory_id_conflicts(clean_data_list):
    for trajectory in clean_data_list:

        subtrajectories = {}
        subtrajectory_cnt = 0
        for trajectory_point in trajectory:

            if len(subtrajectories) == 0:
                # create subtrajectory_id e.g. 23446573_0
                trajectory_point[0] = int(str(int(trajectory_point[0])) + "000" + str(subtrajectory_cnt))
                subtrajectories[trajectory_point[0]] = SubTrajectory(trajectory_point[0], trajectory_point[2], trajectory_point[1])
            else:
                min_distance_to_existing_subtrajectories = global_trajectoryresolutionthreshold + 1
                min_distance_subtrajectory = None

                for subtrajectory in subtrajectories:
                    dist_to_subtrajectory = euclidean_distance(subtrajectories[subtrajectory].last_point_x, subtrajectories[subtrajectory].last_point_y, trajectory_point[2], trajectory_point[1])

                    if dist_to_subtrajectory < min_distance_to_existing_subtrajectories:
                        min_distance_to_existing_subtrajectories = dist_to_subtrajectory
                        min_distance_subtrajectory = subtrajectories[subtrajectory].id

                if min_distance_subtrajectory != None:
                    trajectory_point[0] = min_distance_subtrajectory
                    subtrajectories[min_distance_subtrajectory].replace_last_point(trajectory_point[2], trajectory_point[1])
                else:
                    subtrajectory_cnt += 1
                    trajectory_point[0] = int(str(int(trajectory_point[0])) + "000" + str(subtrajectory_cnt))
                    subtrajectories[trajectory_point[0]] = SubTrajectory(trajectory_point[0], trajectory_point[2], trajectory_point[1])
                    # print(trajectory_point[0])

    df_clean = convert_list_to_dataframe(clean_data_list)

    # re-sort after trajectory id resolution
    print("Re-sorting dataset by trajectory...")
    clean = sort_data_by_trajectory(df_clean, global_column_name_traj_id, global_column_name_time)

    df_clean = convert_list_to_dataframe(clean)

    return df_clean


class SubTrajectory(object):
    def __init__(self, subtraj_id, x, y):
        self.id = subtraj_id
        self.last_point_x = x
        self.last_point_y = y

    def replace_last_point(self, x, y):
        self.last_point_x = x
        self.last_point_y = y


def euclidean_distance(from_x, from_y, to_x, to_y):
    diff_x = to_x - from_x
    diff_y = to_y - from_y
    return math.sqrt(math.pow(diff_x, 2) + math.pow(diff_y, 2))

